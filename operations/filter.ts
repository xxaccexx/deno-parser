import { Pattern } from "../parser.ts";
import { replaceState } from "../state.ts";

// TODO: move to maps

type FilterFn = (result: any, index: number) => boolean;
export const filter = (pattern: Pattern, filter: FilterFn): Pattern => state => {
	const nState = pattern(state);

	return replaceState(
		nState,
		nState.results.filter(filter),
		nState.index,
	)
}