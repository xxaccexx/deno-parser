import { ParserError, type Pattern } from "../mod.ts";

// TODO: create a pattern for handling scoped variables...
export const createReferenceStore = <T = string>(reserved: T[] = []) => {
	const references = new Set<T>();

	const declare = (pattern: Pattern): Pattern => {
		return state => {
			const nState = pattern(state);
			const ref = nState.results.at(-1) as T;

			if (reserved.includes(ref)) {
				throw new ParserError(state, "Reference is reserved");
			}

			references.add(ref);
			return nState;
		}
	}

	const lookup = (pattern: Pattern): Pattern => {
		return state => {
			const nState = pattern(state);
			const ref = nState.results.at(-1) as T;

			if (reserved.includes(ref)) {
				throw new ParserError(state, "Reference is reserved");
			}
			if (!references.has(ref)) {
				throw new ParserError(state, "Reference is not declared");
			}

			return nState;
		}
	}

	return {
		declare,
		lookup,
	}
}