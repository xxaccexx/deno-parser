import { Pattern } from "../parser.ts";
import { replaceState } from "../state.ts";

// TODO: Move to maps

export const flat = (pattern: Pattern, depth?: number): Pattern => state => {
	const nState = pattern(state);

	return replaceState(
		nState,
		nState.results.flat(depth),
		nState.index,
	)
}