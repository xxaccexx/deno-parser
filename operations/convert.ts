import { Pattern } from "../parser.ts";
import { map } from "./map.ts";

export const toInteger = (pattern: Pattern) => map(pattern, i => parseInt(i));

export const toFloat = (pattern: Pattern) => map(pattern, i => parseFloat(i));