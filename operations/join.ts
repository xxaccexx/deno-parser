import { Pattern } from "../parser.ts";
import { replaceState } from "../state.ts";

// TODO: add to maps

export const join = (pattern: Pattern): Pattern => state => {
	const nState = pattern(state);
	return replaceState(
		nState,
		[(nState.results as string[]).join('')],
		nState.index
	)
}