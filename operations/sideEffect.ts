import { Pattern } from "../mod.ts";
import { cloneState } from "../state.ts";
import { ParserState } from '../state.ts'

export const sideEffect = (pattern: Pattern, success: (state: ParserState) => void): Pattern => {
	return state => {
		const nState = pattern(state);
		success(cloneState(nState));
		return nState;
	}
}