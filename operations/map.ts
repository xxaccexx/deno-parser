import type { Pattern } from "../parser.ts";

export type MapFn <I = any, O = any> = (n: I, i: number, a: I[]) => O;

export const map = (pattern: Pattern, mapFn: MapFn): Pattern => state => {
	const nextState = pattern(state);
	return {
		...nextState,
		results: nextState.results.map(mapFn),
	}
}