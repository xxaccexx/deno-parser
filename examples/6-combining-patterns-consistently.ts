import { Pattern, parse } from "../parser.ts";

import { flat } from "../operations/mod.ts";
import { chain, choice, ignore, str } from "../patterns/mod.ts";

// expects either 'AA' or 'D' strings
const batteryType = () => choice(
	str('AA'),
	str("D")
);

// either spelling is fine
const batterySpelling = () => choice(
	str("batteries"),
	str("battery")
)

// expects either battery type combined with either spelling
const getBatteryTypeSpelling = (): Pattern => flat(
	chain(
		batteryType(), 		// get the battery type
		ignore(str(" ")),
		batterySpelling()	// get the battery word
	)
)

const result1 = parse("AA batteries are a common portable power source.", getBatteryTypeSpelling());
const result2 = parse("D battery please", getBatteryTypeSpelling());

console.log(result1); //> ["AA", "batteries"]
console.log(result2); //> ["D", "battery"]