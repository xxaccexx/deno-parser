import { assertEquals } from "../deps.ts";
import { createState } from "../state.ts";
import { only_A_char } from "./2-simple-pattern.ts";

const oldState = createState('A once in a lifetime experience.');
const newState = only_A_char(oldState);

assertEquals(
	newState,
	{
		chunk: 'A once in a lifetime experience.',
		index: 1,
		results: ['A']
	}
);