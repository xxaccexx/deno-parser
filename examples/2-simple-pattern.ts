import { Pattern } from "../parser.ts"
import { appendState, ParserState, ParserError } from "../state.ts";

export const only_A_char: Pattern = (state: ParserState) => {
	if (state.chunk[state.index] === 'A') {
		return appendState(state, ['A'], state.index + 1)
	}
	else {
		throw new ParserError(state, `Expected 'A' but got '${state.chunk[state.index]}'`);
	}
}