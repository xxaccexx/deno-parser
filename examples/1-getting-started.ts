// Take a string of numeric characters and return as a single result.
import { parse } from "../parser.ts";
import { join } from "../operations/mod.ts";
import {
	choice,
	ignore,
	many,
	numChar,
	str
} from "../patterns/mod.ts";

const numberPattern = join(
	many(
		choice(
			numChar(),
			ignore(
				str(',')
			)
		)
	)
);

const results = parse("1,234 results in your search", numberPattern);
console.log(results); //> ["1234"]