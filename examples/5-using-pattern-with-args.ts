import { parse } from "../parser.ts";
import { double_specific_char } from "./4-pattern-with-args.ts";

// expects that there are 2 of a pattern
const result = parse("AA batteries are a common portable power source.", double_specific_char('A'));
console.log(result); //> ["A", "A"]