import { Pattern } from "../parser.ts"
import { str } from "../patterns/string.ts";
import { ParserState, ParserError } from "../state.ts";

export const double_specific_char = (char: string): Pattern => {
	if (char.length !== 1) throw new Error('Only one char allowed');

	return (state: ParserState) => {
		try {
			const firstMatchState = str(char)(state);
			const secondMatchState = str(char)(firstMatchState);

			return secondMatchState;
		}
		catch {
			throw new ParserError(state, `Expected '${char}' but got '${state.chunk[state.index]}'`);
		}
	}
}