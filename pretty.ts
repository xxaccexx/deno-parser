export const prettyChars = (actual: string) => {
	switch (actual) {
		case '\n': return '\\n';
		case '\t': return '\\t';
		case '\r': return '\\r';
		default: return actual;
	}
}