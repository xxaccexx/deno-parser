import { assertEquals, assertThrows } from "../deps.ts"

import { createState } from "../state.ts"
import { str } from "../patterns/string.ts"
import { take } from "../patterns/take.ts"
import { until } from "../patterns/until.ts"

const state = createState('abcdefg')

Deno.test({ name: 'passes immediately if until passes', async fn() {
	const pattern = until(
		take(1),
		str('a')
	)
	const actual = pattern(state)

	assertEquals(actual.results, [])
	assertEquals(actual.index, 0)
}})

Deno.test({ name: 'passes after 1 match result', async fn() {
	const pattern = until(
		take(1),
		str('b')
	)
	const actual = pattern(state)

	assertEquals(actual.results, [['a']])
	assertEquals(actual.index, 1)
}})

Deno.test({ name: 'passes after many match results', async fn() {
	const pattern = until(
		take(1),
		str('g')
	)
	const actual = pattern(state)

	assertEquals(actual.results, [['a'], ['b'], ['c'], ['d'], ['e'], ['f']])
	assertEquals(actual.index, 6)
}})

Deno.test({ name: 'state is correct after multiple passes', async fn() {
	const pattern1 = until(
		take(3),
		str('d')
	)
	const state1 = pattern1(state)

	assertEquals(state1.results, [['abc']])
	assertEquals(state1.index, 3)

	const pattern2 = until(
		take(3),
		str('g')
	)
	const state2 = pattern2(state)

	assertEquals(state2.results, [['abc'], ['def']])
	assertEquals(state2.index, 6)
}})

Deno.test({ name: 'passes if until never passes', async fn() {
	const pattern = until(
		take(1),
		str('z')
	)
	const actual = pattern(state);

	assertEquals(actual.results, [['a'], ['b'], ['c'], ['d'], ['e'], ['f'], ['g']])
	assertEquals(actual.index, 7)
}})

Deno.test({ name: 'fails if match does not pass', async fn() {
	const pattern = until(
		str('a'),
		str('z')
	)
	assertThrows(() => pattern(state))
}})
