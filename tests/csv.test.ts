import { assertEquals } from "../deps.ts";

import { createState } from "../state.ts"
import { csvStringCell, csv, psv, tsv } from "../formats/csv.ts"

Deno.test({ name: 'cell - can match bare cell value', async fn() {
	const pattern = csvStringCell();
	const actual = pattern(createState("foo"));
	assertEquals(actual.results, ['foo'])
}})

Deno.test({ name: 'cell - can match quoted cell value', async fn() {
	const pattern = csvStringCell();
	const actual = pattern(createState("foo"));
	assertEquals(actual.results, ['foo'])
}})

Deno.test({ name: 'cell - can match if we use alternative quote character', async fn() {
	const pattern = csvStringCell({ quote: "`" });
	const actual = pattern(createState("`foo`"));
	assertEquals(actual.results, ["foo"])
}})



Deno.test({ name: 'row - csv row', async fn() {
	const pattern = csv();
	const csvState = pattern(createState('"foo",bar,"foo,bar"\n'));
	assertEquals(csvState.results, ["foo", "bar", "foo,bar"])
}})

Deno.test({ name: 'row - tsv row', async fn() {
	const pattern = tsv();
	const csvState = pattern(createState('foo\t"bar"\t"foo\tbar"\n'));
	assertEquals(csvState.results, ["foo", "bar", "foo\tbar"])
}})

Deno.test({ name: 'row - psv row', async fn() {
	const pattern = psv();
	const csvState = pattern(createState('foo|`bar`|`foo|bar`\n'));
	assertEquals(csvState.results, ["foo", "bar", "foo|bar"])
}})