import {
	chain,
	many,
	str,
	until,
	take,
} from "../patterns/mod.ts";

import { ParserError, createState } from "../mod.ts";
import { assertEquals } from "@std/assert/assert_equals.ts";

import { sideEffect } from '../operations/sideEffect.ts';
import { createReferenceStore } from '../operations/reference.ts';
import { ignore } from "../patterns/ignore.ts";
import { join } from "../operations/join.ts";
import { flat } from "../operations/flat.ts";
import { assert } from "@std/assert/assert.ts";
import { assertThrows } from "@std/assert/assert_throws.ts";


Deno.test({ name: 'Side effect - Can collect states as we go', fn() {
	const input = createState("aa");
	const list: string[] = [];

	const pattern = many(
		sideEffect(
			str("a"),
			state => list.push(state.results.at(-1))
		)
	);

	pattern(input);

	assertEquals(list, ['a','a'])
}})


Deno.test({ name: 'references - declare then access', fn() {
	const { declare, lookup } = createReferenceStore();

	const testDeclare = declare(str("const"));
	const testLookup = lookup(str("const"));

	testDeclare(createState("const"));
	testLookup(createState("const"));
}})

Deno.test({ name: 'references - cannot use reserved reference', fn() {
	const { declare, lookup } = createReferenceStore(["const"]);

	const testDeclare = declare(str("const"));
	const testLookup = lookup(str("const"));

	assertThrows(() => testDeclare(createState("const")), ParserError, "Reference is reserved")
	assertThrows(() => testLookup(createState("const")), ParserError, "Reference is reserved")
}})

Deno.test({ name: 'references - cannot use reserved reference', fn() {
	const { lookup } = createReferenceStore();
	const testLookup = lookup(str("const"));

	assertThrows(() => testLookup(createState("const")), ParserError, "Reference is not declared")
}})