import { assertEquals } from "../deps.ts"

import { createState } from "../state.ts";
import { chain, ignore, str } from "../patterns/mod.ts";
import { flat } from "../operations/flat.ts"

const state = createState('abcdefg');

Deno.test({ name: 'ignored patterns are not included in results', async fn() {
	const pattern = flat(
		chain(
			str('a'),
			str('b'),
			ignore(str('c')),
			str('d'),
		)
	)

	const actual = pattern(state);

	assertEquals(actual.results, ['a', 'b', 'd']);
}})