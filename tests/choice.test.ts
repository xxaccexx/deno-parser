import { assertEquals, assertThrows } from '../deps.ts'

import { createState } from "../state.ts";
import { choice } from "../patterns/choice.ts";
import { str } from "../patterns/string.ts";

const state = createState('abcdefg');

Deno.test({ name: 'Will take the first matching pattern', async fn() {
	const pattern = choice(
		str("abc"),
		str("ab"),
	);
	const actual = pattern(state);

	assertEquals(actual.results, ["abc"])
	assertEquals(actual.index, 3)
}})

Deno.test({ name: 'ignores failing matches', async fn() {
	const pattern = choice(
		str("123"),
		str("abc"),
	);
	const actual = pattern(state);

	assertEquals(actual.results, ["abc"])
	assertEquals(actual.index, 3)
}})

Deno.test({ name: 'fails if no matches', async fn() {
	const pattern = choice(
		str("123"),
		str("999"),
		str("cba"),
	)

	assertThrows(() => pattern(state))
}})
