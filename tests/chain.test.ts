import { assertEquals, assertThrows } from "../deps.ts"

import { chain } from "../patterns/chain.ts"
import { str } from "../patterns/string.ts"
import { createState } from "../state.ts"

const state = createState('abcdefg')

Deno.test({ name: 'take a group of patterns in order', async fn() {
	const pattern = chain(
		str('a'),
		str('b'),
		str('c'),
	)
	const actual = pattern(state)

	assertEquals(actual.results, [['a'], ['b'], ['c']])
	assertEquals(actual.index, 3)
}})

Deno.test({ name: 'failes if any failed', async fn() {
	const pattern = chain(
		str('a'),
		str('b'),
		str('d'),
	)

	assertThrows(() => pattern(state));
}})

Deno.test({ name: 'state is correct over multiple patterns', async fn() {
	const pattern1 = chain(
		str('a'),
		str('b'),
		str('c'),
	)
	const state1 = pattern1(state)

	assertEquals(state1.results, [['a'], ['b'], ['c']])
	assertEquals(state1.index, 3)

	const pattern2 = chain(
		str('d'),
		str('e'),
		str('f'),
	)
	const state2 = pattern2(state1)

	assertEquals(state2.results, [['a'], ['b'], ['c'], ['d'], ['e'], ['f']])
	assertEquals(state2.index, 6)
}})