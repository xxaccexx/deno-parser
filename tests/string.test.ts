import { assertEquals, assertThrows } from "../deps.ts";

import { ParserError, createState } from "../state.ts";
import { str } from "../patterns/string.ts";

const state = createState('abcdefg');

Deno.test({ name: 'string - Match 1 char', async fn() {
	const pattern = str('a');
	const actual = pattern(state);

	assertEquals(actual.results, ['a']);
	assertEquals(actual.index, 1);
}})

Deno.test({ name: 'string - Match 3 chars', async fn() {
	const pattern = str('abc');
	const actual = pattern(state);

	assertEquals(actual.results, ['abc']);
	assertEquals(actual.index, 3);
}})

Deno.test({ name: 'string - Match all chars', async fn() {
	const pattern = str('abcdefg');
	const actual = pattern(state);

	assertEquals(actual.results, ['abcdefg']);
	assertEquals(actual.index, 7);
}})

Deno.test({ name: 'string - state is correct over multiple patterns', async fn() {
	const pattern1 = str('abc');
	const state1 = pattern1(state);

	assertEquals(state1.results, ['abc']);
	assertEquals(state1.index, 3);

	const pattern2 = str('def');
	const state2 = pattern2(state1);

	assertEquals(state2.results, ['abc', 'def']);
	assertEquals(state2.index, 6)
}})

Deno.test({ name: 'string - Match escaped chars', async fn() {
	const pattern = str('\n');
	const actual = pattern(createState('\n'));

	assertEquals(actual.results, ['\n']);
	assertEquals(actual.index, 1);
}})

Deno.test({ name: 'string - fails when not matching', async fn() {
	const pattern = str('123');
	assertThrows(() => pattern(state), ParserError, "Expected '123' but got 'abc'")
}})

Deno.test({ name: 'string - fails when over matching', async fn() {
	const pattern = str('abcdefgh');
	assertThrows(() => pattern(state), ParserError, "Expected 'abcdefgh' but got 'abcdefg'")
}})