import { assertEquals } from '../deps.ts'

import {
	appendState,
	emptyState,
	createState,
	replaceState,
} from "../state.ts"

Deno.test({ name: 'state - create state', fn() {
	const state = createState('known')

	assertEquals(state.chunk, 'known')
	assertEquals(state.results, [])
	assertEquals(state.index, 0)
}})

Deno.test({ name: 'state - append state', fn() {
	let state = createState('known');
	state = appendState(state, ['first'], 1)
	state = appendState(state, ['second'], 2)
	state = appendState(state, ['third'], 3)

	assertEquals(state.chunk, 'known')
	assertEquals(state.results, ['first', 'second', 'third'])
	assertEquals(state.index, 3)
}})

Deno.test({ name: 'state - replace state', fn() {
	let state = createState('known');
	state = replaceState(state, ['first'], 1)
	state = replaceState(state, ['second'], 2)
	state = replaceState(state, ['third'], 3)

	assertEquals(state.chunk, 'known')
	assertEquals(state.results, ['third'])
	assertEquals(state.index, 3)
}})

Deno.test({ name: 'state - clone state', fn() {
	let state = createState('known');
	state = appendState(state, ['first'], 1)
	state = appendState(state, ['second'], 2)
	state = appendState(state, ['third'], 3)

	const clone = emptyState(state);

	assertEquals(clone.chunk, 'known')
	assertEquals(clone.results, [])
	assertEquals(clone.index, 3)
}})