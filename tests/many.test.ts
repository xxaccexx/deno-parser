import { assertEquals, assertThrows } from "../deps.ts";


import { createState } from "../state.ts";
import {
	choice,
	ignore,
	str,
	take,
	many
} from "../patterns/mod.ts";


Deno.test({ name: 'Many - Takes all', fn() {
	const pattern = many(take());
	const state = pattern(createState("abc"));

	assertEquals(state.results, ['a', 'b', 'c'])
}})

Deno.test({ name: 'Many - Str matches until it doesnt', fn() {
	const pattern = many(str('a'));
	const state = pattern(createState("aaabaaa"));

	assertEquals(state.results, ['a','a','a'])
}})

Deno.test({ name: 'Many - Throws if min not reached', fn() {
	const pattern = many(str('a'), 4);
	assertThrows(() => pattern(createState('aaabaa')));
}})

Deno.test({ name: 'Many - choices work many times', fn() {
	const pattern = many(choice(str('a'), ignore(str('b'))));
	const state = pattern(createState("aaabaaba"));

	assertEquals(state.results, ['a','a','a','a','a','a'])
}})