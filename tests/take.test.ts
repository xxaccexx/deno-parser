import { assertEquals } from "../deps.ts";

import { take } from "../patterns/take.ts";
import { createState } from "../state.ts";

const state = createState("abcdefg")

Deno.test({ name: 'take - Take can consume across chunks', async fn() {
	const pattern = take(1);
	const actual = pattern(state);

	assertEquals(actual.results, ['a']);
	assertEquals(actual.index, 1);
}})

Deno.test({ name: 'take - Take fails when there arent enough chars in the file', async fn() {
	const pattern = take(3);
	const actual = pattern(state);

	assertEquals(actual.results, ['abc']);
	assertEquals(actual.index, 3);
}})

Deno.test({ name: 'string - state is correct over multiple patterns', async fn() {
	const pattern1 = take(3);
	const state1 = pattern1(state);

	assertEquals(state1.results, ['abc']);
	assertEquals(state1.index, 3);

	const pattern2 = take(3);
	const state2 = pattern2(state1);

	assertEquals(state2.results, ['abc', 'def']);
	assertEquals(state2.index, 6)
}})