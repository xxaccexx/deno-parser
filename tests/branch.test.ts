import { assertEquals } from "../deps.ts"

import { createState } from "../state.ts"
import { branch } from "../patterns/branch.ts"
import { str } from "../patterns/string.ts"
import { take } from "../patterns/take.ts"
import { until } from "../patterns/until.ts"

const state = createState('abcdefg')

Deno.test({ name: 'branch - run then pattern if condition passes', async fn() {
	const pattern = branch(
		str('a'),
		take(3),
		take(1),
	)
	const actual = pattern(state);

	assertEquals(actual.results, ['abc'])
	assertEquals(actual.index, 3)
}})

Deno.test({ name: 'branch - run otherwise pattern if condition fails', async fn() {
	const pattern = branch(
		str('z'),
		take(3),
		take(1),
	)
	const actual = pattern(state);

	assertEquals(actual.results, ['a'])
	assertEquals(actual.index, 1)
}})

Deno.test({ name: 'branch - with until', async fn() {
	const state = createState('"abcdefg"');
	const pattern = branch(
		str('"'),
		until(
			take(1),
			str('c')
		),
		take(1),
	)
	const actual = pattern(state);

	assertEquals(actual.results, [['"'], ['a'], ['b']])
	assertEquals(actual.index, 3)
}})
