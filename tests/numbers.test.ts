import { assertEquals, assertThrows } from "../deps.ts";
import { ParserError, createState } from "../state.ts";

import {
	exponent,
	integer,
} from '../patterns/number.ts';

import {
	branch,
	chain,
	optional,
	str,
	until,
	binChar,
	numChar,
	hexChar,
} from "../patterns/mod.ts";

import {
	join,
	flat,
	toFloat,
	toInteger
} from "../operations/mod.ts";


const binChars = ['0', '1'];
const numChars = ['2', '3', '4', '5', '6', '7', '8', '9'];
const hexChars = ['a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'];
const randChars = ['*', '!', '_', '-', '.'];

Deno.test({ name: 'Number - binary chars', fn() {
	const pattern = binChar();

	binChars.forEach(char => {
		const state = pattern(createState(char));
		assertEquals(state.results, [char]);
	});

	[
		...numChars,
		...hexChars,
		...randChars,
	].forEach(char =>
		assertThrows(() => pattern(createState(char)), ParserError)
	)
}});

Deno.test({ name: 'Number - num chars', fn() {
	const pattern = numChar();

	[
		...binChars,
		...numChars,
	].forEach(char => {
		const state = pattern(createState(char));
		assertEquals(state.results, [char]);
	});

	[
		...hexChars,
		...randChars,
	].forEach(char =>
		assertThrows(() => pattern(createState(char)), ParserError)
	)
}});

Deno.test({ name: 'Number - hex chars', fn() {
	const pattern = hexChar();

	[
		...binChars,
		...numChars,
		...hexChars,
	].forEach(char => {
		const state = pattern(createState(char));
		assertEquals(state.results, [char]);
	});

	[
		...randChars,
	].forEach(char =>
		assertThrows(() => pattern(createState(char)), ParserError)
	)
}});

Deno.test({ name: 'Number - exponent', fn() {
	const pattern = exponent();

	assertEquals(
		pattern(createState('e2')).results,
		['e2']
	)

	assertThrows(
		() => pattern(createState('e2.123')),
		ParserError
	)

	assertEquals(
		pattern(createState('e2 .123')).results,
		['e2']
	)
}})


Deno.test({ name: 'Number - Parse integer', fn() {
	const pattern = integer();

	assertEquals(
		pattern(createState('1')).results,
		['1']
	)

	assertEquals(
		pattern(createState('123')).results,
		['123']
	)

	assertEquals(
		pattern(createState('0')).results,
		['0']
	)

	assertThrows(
		() => pattern(createState('.1')),
		ParserError
	)
}})

Deno.test({ name: 'Number - Map to integer', fn() {
	const decimals = () => chain(
		str('.'),
		until(
			numChar(),
			str('eof')
		)
	);

	const integers = () => until(
		numChar(),
		str('.')
	);

	const intOrFloat = () => join(
		flat(
			branch(
				str('.'),
				decimals(),
				chain(
					integers(),
					optional(decimals()),
				)
			)
		)
	)

	assertEquals(
		toInteger(intOrFloat())(createState('123123123')).results,
		[123123123]
	)

	assertEquals(
		toFloat(intOrFloat())(createState('.123')).results,
		[.123]
	)

	assertEquals(toInteger(intOrFloat())(createState('123.')).results, [123])
	assertThrows(() => toInteger(intOrFloat())(createState('123asdf')), ParserError)
}})