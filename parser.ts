import { ParserError, ParserState, createState } from "./state.ts";


export type Pattern = (state: ParserState) => ParserState;

export const parse = (input: string, pattern: Pattern) => {
	const state = pattern(createState(input));
	return state.results;
}

export const printError = (error: ParserError) => {
	console.error('Parser Error:', error.message, 'at', error.state.index);
	console.error(error.state.chunk);

	const arr = [...new Array(error.state.index - 1)].map(_ => '-');
	const pointer = [...arr, '^'].join('');
	console.error(pointer);
}