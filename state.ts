export type ParserState = {
	chunk: string;
	index: number;
	results: any[];
}

export class ParserError extends Error {
	state: ParserState;
	stateString: string;

	constructor(state: ParserState, msg: string) {
		super(msg);
		this.state = state;
		this.stateString = Deno.inspect(state, {
			colors: true,
			depth: Infinity
		});
	}
}

export const createState = (chunk: string): ParserState => ({
	chunk,
	index: 0,
	results: [],
})

export const appendState = (oldState: ParserState, results: any[], newIndex: number): ParserState => ({
	...oldState,
	index: newIndex,
	results: [...oldState.results, ...results],
})

export const replaceState = (oldState: ParserState, results: any[], newIndex: number): ParserState => ({
	...oldState,
	index: newIndex,
	results,
})

export const emptyState = (oldState: ParserState): ParserState => ({
	...oldState,
	results: [],
})

export const cloneState = (sourceState: ParserState) => structuredClone(sourceState);