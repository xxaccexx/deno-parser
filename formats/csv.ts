import { Pattern } from "../parser.ts";

import {
	branch,
	chain,
	choice,
	str,
	take,
	until,
	optional,
} from "../patterns/mod.ts";

import {
	join,
	flat,
	filter,
} from "../operations/mod.ts";


type Quote = '"' | "'" | '`' | string;
type Delimeter = ',' | '\t' | '|' | string;

type CSVOptions = {
	quote: Quote;
	delim: Delimeter;
}


/** @deprecated */
export const csvStringCell = ({ quote = '"', delim = ',' }: Partial<CSVOptions> = {}): Pattern => (
	join(
		branch(
			str(quote),

			filter(
				flat(
					chain(
						str(quote),
						until(
							take(1),
							str(quote)
						),
						str(quote)
					),
					2
				),
				(i: string) => i !== quote
			),

			until(
				take(1),
				choice(
					str(delim),
					str('\n')
				)
			)
		)
	)
)

/** @deprecated */
export const seperatedValues = ({ quote = '"', delim = ',' }: Partial<CSVOptions> = {}): Pattern => flat(
	until(
		filter(
			flat(
				chain(
					csvStringCell({ quote, delim }),
					optional(str(delim))
				)
			),
			(i: string) => i !== delim
		),
		str('\n')
	)
)

/** @deprecated */
export const csv = () => seperatedValues({ quote: '"', delim: ',' })

/** @deprecated */
export const tsv = () => seperatedValues({ quote: '"', delim: '\t' })

/** @deprecated */
export const psv = () => seperatedValues({ quote: '`', delim: '|' })