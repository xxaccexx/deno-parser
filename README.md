# Deno Parse

This is a simple, yet robust string parser. This is the barebones structure to doing more complex document parsing.

The pattern this library uses is called 'Parser combinators' which allows for composing more complex patterns out of building simpler blocks of patterns.

[Getting Started Example](./examples/1-getting-started.ts)
```ts
// Take a string of numeric characters and return as a single result.
// Take a string of numeric characters and return as a single result.
import { parse } from "../parser.ts";
import { join } from "../operations/mod.ts";
import {
	choice,
	ignore,
	many,
	numChar,
	str
} from "../patterns/mod.ts";

const numberPattern = join(
	many(
		choice(
			numChar(),
			ignore(
				str(',')
			)
		)
	)
);

const results = parse("1,234 results in your search", numberPattern);
console.log(results); //> ["1234"]
```

Included in this project are:
* Basic patterns like `take`, `str`, `choice`, `many`, etc, etc.
* Basic higher order operations like `map` and `join` to combine or redeclare parsed values.
* Functions and Types for maintaining state, such as `replaceState` and `ParserError`.

Deprecated:
* (g)unzip and other operations for parsing content in streams.
* Comma, Tab and Pipe seperated value patterns, aka: CSV, TSV and PSV parser.<br/>
	You can find this on [watch this space](https://gitlab.com/xxaccexx/deno-seperated-values)

These all use a `Pattern` function interface adding more consistency to the pattern definitions.

# Basic types

## State

The `ParserState` type is a simple object that keeps the chunk of content it is operating on, the index and the results it has parsed.

```ts
type ParserState = {
	index: number;
	chunk: string;
	results: any[];
}
```

`ParserState` objects are passed between patterns and modified in an immutable way. This means you can grab any state at any point and be sure it will not change at the end of the parse.

There are a few pure functions that allow creating new state based on previous state.


## Pattern

A `Pattern` is a function that takes state and returns a new, immutable state.

[Simple Pattern Example](./examples/2-simple-pattern.ts)
```ts
import { Pattern } from "../parser.ts"
import { appendState, ParserState, ParserError } from "../state.ts";

export const only_A_char: Pattern = (state: ParserState) => {
	if (state.chunk[state.index] === 'A') {
		return appendState(state, ['A'], state.index + 1)
	}
	else {
		throw new ParserError(state, `Expected 'A' but got '${state.chunk[state.index]}'`);
	}
}
```

### Using a pattern.

Using patterns is very simple. Just call it with a state to get an updated state.

[Using a Pattern Example](./examples/3-use-a-pattern.ts)
```ts
import { assertEquals } from "../deps.ts";
import { createState } from "../state.ts";
import { only_A_char } from "./examples/2-simple-pattern.ts";

const oldState = createState('A once in a lifetime experience.');
const newState = only_A_char(oldState);

assertEquals(
	newState,
	{
		chunk: 'A once in a lifetime experience.',
		index: 1,
		results: ['A']
	}
);
```

While this works, it is likely you will want to provide variables to your pattern, such as a pattern that checks for numeric characters with some sort of min/max pattern.<br/>
For this sort of pattern abstraction, you can use higher order functions that returns a `Pattern` function.

[Pattern with arguments](./exmaples/4-pattern-with-args.ts)
```ts
import { Pattern } from "../parser.ts"
import { appendState, ParserState, ParserError } from "../state.ts";

export const one_specific_char = (char: string): Pattern => {
	if (char.length !== 1) throw new Error('Only one char allowed');

	return (state: ParserState) => {
		if (state.chunk[state.index] === char) {
			return appendState(state, [char], state.index + 1)
		}
		else {
			throw new ParserError(state, `Expected '${char}' but got '${state.chunk[state.index]}'`);
		}
	}
}
```

And if you want to use another pattern as an argument, you can just pass the state to the pattern.

You can do that as many times as you like, but make sure to use the updated state, instead of the original one.<br/>
Just remember the state is supposed to be immutable and the core functioanlity expects that, but does not inforce it.

(Using a pattern with args)[./examples/5-using-patter-with-args.ts]
```ts
import { parse } from "../parser.ts";
import { double_specific_char } from "./4-pattern-with-args.ts";

// expects that there are 2 of a pattern
const result = parse("AA batteries are a common portable power source.", double_specific_char('A'));
console.log(result); //> ["A", "A"]
```

A strong convention is to declare your patterns as higher order functions, even if you don't need the abstraction.<br/>
This adds some constency to your patterns and this is the pattern this library uses.


### Combining patterns

This example shows how a new pattern can be created out of combining other patterns.<br/>
This pattern with match either 'AA' or 'D' characters when combined with a single or plural battery word.<br/>
This gives 4 possible outputs from that pattern.

(Combining patterns consistently example)[./examples/6-combining-patterns-consistently.ts]
```ts
import { Pattern, parse } from "../parser.ts";

import { flat } from "../operations/mod.ts";
import { chain, choice, ignore, str } from "../patterns/mod.ts";

// expects either 'AA' or 'D' strings
const batteryType = () => choice(
	str('AA'),
	str("D")
);

// either spelling is fine
const batterySpelling = () => choice(
	str("batteries"),
	str("battery")
)

// expects either battery type combined with either spelling
const getBatteryTypeSpelling = (): Pattern => flat(
	chain(
		batteryType(), 		// get the battery type
		ignore(str(" ")),
		batterySpelling()	// get the battery word
	)
)

const result1 = parse("AA batteries are a common portable power source.", getBatteryTypeSpelling());
const result2 = parse("D battery please", getBatteryTypeSpelling());

console.log(result1); //> ["AA", "batteries"]
console.log(result2); //> ["D", "battery"]
```


# API

## State

* `createState(str)` creates a state object from a string.<br/>
	the `chunk` property will be set to `str`, and `index` is `0` and `results` is `[]` by default.

* `appendState(oldState, additionalResults, newIndex)` returns a new state object,<br/>
	The chunk will be unchanged, which is usually what you want.<br/>
	The `results` will be concatinated and the `index` will be set to the `newIndex`

* `replaceState(oldState, newResults, newIndex)` returns a new state object,<br/>
	the chunk will be unchanged, which is usually what you want.<br/>
	The `results` will be set to `newResults` and `index` will be set to `newIndex`

* `cloneState(oldState)` returns a duplicate of state unchanged from `oldState`,<br/>
	except for `results` which is set back to the initial value `[]`

## Patterns

All these are higher order functions, and return the `Pattern` function.

A good pattern is to declare your pattern before, as a `const myPattern = many(take(1)); myPattern(createState("abcdefg"));`


* `take(count: number): Pattern`<br/>
	Will take the next `count` of characters from the chunk and appends to results. Also increments index by `count`.<br/>
	Throws if there are less than `count` characters remaining in the chunk.<br/>
	"I expect there are `count` number of characters, but I don't care what they are"

* `str(exact: string): Pattern`<br/>
	Matches if `exact` is the next text in the chunk. Append `exact` and increment index by `exact` length.<br/>
	"I want `exact`ly this string"

* `optional(pattern: Pattern): Pattern`<br/>
	Trys to match `pattern` and append to state. Silence any errors.<br/>
	"I think this `pattern` might be here, but if it's not just keep going like nothing happened"

* `choice(...options: Pattern[]): Pattern`<br/>
	Loops over patterns and returns the state of the first to match successfully.<br/>
	If no options match, then throws `ParserError(state, 'No patterns matched')`<br/>
	"One of these patterns _should_ work"

* `chain(...sequence: Pattern[]): Pattern`<br/>
	Runs all the patterns in `sequence`, in order, and will throw if any throw.<br/>
	Just a loop over known patterns in order.

* `many(pattern: Pattern, min?: number = 1): Pattern`<br/>
	Keep matching `pattern` until you can't anymore, with a minimum of `min`<br/>
	If you don't match `pattern` `min` times, then throw `ParserError(state, 'Minimum matches not reached')`<br/>
	"I have a lot of this pattern one after the other and all of them should match"

* `until(match: Pattern, end: Pattern)`<br/>
	Keep matching `match` until the `end` pattern is matched.<br/>
	`until` will only append the results from `match` and not the `end` so if you expect `end` as the results,<br/>
	combine with `chain(until(match, end), end)`<br/>
	Similar to `many`, but with a stop condition.

* `branch(condition: Pattern, then: Pattern, otherwise: Pattern): Pattern`<br/>
	This will check if condition parses, and then returns the result of `then`<br/>
	If the `condition` throws, then branch returns the results from `otherwise` instead.<br/>
	Basically an if/else branch.

* `ignore(pattern: Pattern[]): Pattern`<br/>
	Will progress state by the same amount as pattern, but will not append the result.<br/>
	"Expect this pattern, but don't add it to the results"

* `log(child: Pattern, logger: (state: ParserState) => any[]): Pattern`<br/>
	Log out the state after parsing with `child`. You can provide the items by returning an array from `logger`.<br/>
	TODO: Add a way to disable this for production, or based on log levels. Its probably best to not use this in production.
