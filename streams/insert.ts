import { BigQuery } from 'npm:@google-cloud/bigquery';
import { RecordWithSchema } from './schema.ts';

type BQConfig = {
	projectId: string;
	datasetId: string;
	tableId: string;
	keyFilename: string;
	location?: 'australia-southeast1'
}

/** @deprecated */
export const insertStream = async (options: BQConfig) => {
	const {
		projectId,
		datasetId,
		tableId,
		keyFilename,
		location = 'australia-southeast1'
	} = options;

	const bq = new BigQuery({ projectId, keyFilename });

	const dataset = bq.dataset(datasetId, { location });
	const [datasetExists] = await dataset.exists();
	if (!datasetExists) await dataset.create({ location });

	const table = dataset.table(tableId, { location });
	const [tableExists] = await table.exists();
	if (!tableExists) await table.create({ location });

	const writable = new WritableStream<RecordWithSchema>({
		async write(record) {
			await table.setMetadata({ schema: { fields: record.schema } });
			await table.insert(record.row)
		},
	});

	return writable;
}