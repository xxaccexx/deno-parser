import { Readable, Writable } from 'node:stream'

/** @deprecated */
export const readableToWeb = (nodeReadable: Readable) => {
	const { writable, readable } = new TransformStream();
	const writer = writable.getWriter();

	nodeReadable.on('data', chunk => writer.write(chunk));
	nodeReadable.on('close', () => writer.close());
	nodeReadable.on('error', err => writer.abort(err));

	return readable;
}

/** @deprecated */
export const writableToWeb = (nodeWritable: Writable) => {
	console.log('NOT IMPLEMENTED: writableToWeb Acting as passthrough.')
	return new TransformStream();
}

/** @deprecated */
export const transformToWeb = () => {
	console.log('NOT IMPLEMENTED: transformToWeb Acting as passthrough.')
	return new TransformStream();
}