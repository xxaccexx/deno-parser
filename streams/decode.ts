/** @deprecated */
export const decodeStream = () => {
	const decoder = new TextDecoder();

	return new TransformStream<Uint8Array, string>({
		transform(chunk, ctrl) {
			const cont = decoder.decode(chunk);
			ctrl.enqueue(cont);
		}
	});
}