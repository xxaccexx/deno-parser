/** @deprecated */
export const drainStream = (silent = true) => new WritableStream<any>({
	write(chunk) {
		if (!silent) console.log('DRAIN:', chunk);
	}
})