import { Pattern } from "../parser.ts";
import { createState } from "../state.ts";

/** @deprecated */
export const parserStream = (pattern: Pattern) => new TransformStream<string, string[]>({
	transform(chunk, ctrl) {
		const state = createState(chunk);
		ctrl.enqueue(
			pattern(state).results
		)
	}
})