/** @deprecated */
export const lineBreak = () => {
	const ignore = 13;		// \r
	const delimiter = 10; // \n

	let cache: number[] = [];

	return new TransformStream<Uint8Array, Uint8Array>({
		transform(chunk, ctrl) {
			for (const byte of chunk) {
				if (byte !== ignore) cache.push(byte);

				if (byte === delimiter) {
					ctrl.enqueue(
						Uint8Array.from(cache)
					);
					cache = [];
				}
			}
		},

		flush(ctrl) {
			if (cache.length) {
				ctrl.enqueue(
					Uint8Array.from(cache)
				)
				cache = [];
			}
		}
	})
}
