import {
	readerFromStreamReader,
	iterateReader,
	Untar
} from '../deps.ts'


/** @deprecated */
export const untar = (files: string[]) => {
	const { writable, readable: innerReadable } = new TransformStream<Uint8Array, Uint8Array>();
	const { readable, writable: innerWritable } = new TransformStream<Uint8Array, Uint8Array>();

	const reader = innerReadable.getReader();
	const writer = innerWritable.getWriter();

	const untar = new Untar(readerFromStreamReader(reader));

	(async () => {
		for await (const entry of untar) {
			const includeThis = files.some(file => entry.fileName.includes(file));

			if (includeThis) {
				for await (const chunk of iterateReader(entry)) {
					await writer.write(chunk);
				}
			}
		}

		await writer.close();
	})();

	return { writable, readable }
}