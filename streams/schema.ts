import type { TableSchema } from 'npm:@google-cloud/bigquery'

/** @deprecated */
export type FieldSchema = Exclude<TableSchema['fields'], undefined>[number];

/** @deprecated */
export type RecordWithSchema = {
	row: Record<string, string>;
	schema: FieldSchema[];
}

/** @deprecated */
export const schemaStream = () => {
	let headers: string[] = [];
	let schema: FieldSchema[];

	return new TransformStream<string[], RecordWithSchema>({
		transform(record, ctrl) {
			if (!headers.length) {
				headers = record;

				schema = record.map(name => ({
					name,
					type: 'STRING',
					mode: 'NULLABLE'
				}))
			}

			else {
				ctrl.enqueue({
					schema,
					row: Object.fromEntries(
						headers.map((name, i) => [name, record[i]])
					),
				})
			}
		}
	})
}