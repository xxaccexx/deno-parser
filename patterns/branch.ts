import { Pattern } from "../parser.ts";

/**
 * if pattern matches, follow the 'then' pattern.
 * failing the test will either follow the 'otherwise' pattern, or noop.
 * The test is not included in the pattern. You'll need to match this in the 'then' pattern.
 */
export const branch = (condition: Pattern, then: Pattern, otherwise: Pattern): Pattern => state => {
	try {
		condition(state);
		return then(state);
	}
	catch {
		return otherwise(state);
	}
}
