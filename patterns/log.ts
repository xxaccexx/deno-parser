import { Pattern } from "../parser.ts";
import { ParserState } from "../state.ts";

export const log = (child: Pattern, logger: (state: ParserState) => any[]): Pattern => state => {
	const nState = child(state);

	const msg = logger(nState).map(i => {
		if (typeof i === 'object') return Deno.inspect(i, { colors: true, depth: Infinity });
		return i;
	})

	console.log(...msg);

	return nState;
}