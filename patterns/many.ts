import { Pattern } from "../parser.ts";
import { ParserError } from "../state.ts";

const MAX_STEPS = 2048;

export const many = (pattern: Pattern, min = 1): Pattern => state => {
	let nState = state;

	while (nState.results.length < MAX_STEPS) {
		if (nState.index >= nState.chunk.length) break;

		try {
			nState = pattern(nState);
		}
		catch (e) {
			if (e instanceof ParserError) {
				if (nState.results.length < min) {
					throw new ParserError(nState, 'Minimum matches not reached');
				}

				else {
					break;
				}
			}

			throw e;
		}
	}

	return nState;
}