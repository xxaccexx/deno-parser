import { Pattern } from "../parser.ts";

export const ignore = (pattern: Pattern): Pattern => state => {
	const nextState = pattern(state);

	return {
		...state,
		index: nextState.index,
	}
}