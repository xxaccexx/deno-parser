import { Pattern } from "../parser.ts"
import { ParserError, emptyState, replaceState } from "../state.ts";

const MAX_STEPS = 2048;

/**
 * keep matching the expected pattern until another pattern occurs.
 */
export const until = (match: Pattern, end: Pattern): Pattern => state => {
	let nState = state;
	const results: any[] = [];

	while (nState.results.length < MAX_STEPS) {
		if (nState.index >= nState.chunk.length) break;

		try {
			end(emptyState(nState));
			break;
		}
		catch {
			const matched = match(nState)
			results.push(matched.results)
			nState = emptyState(matched)
		}

		if (results.length > MAX_STEPS) {
			throw new ParserError(
				nState,
				'until can has an upper limit of matches of 255'
			);
		}
	}

	return replaceState(nState, results, nState.index);
}
