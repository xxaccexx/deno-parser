import { ParserError, appendState } from "../state.ts";
import { prettyChars } from "../pretty.ts";
import { Pattern } from "../parser.ts";

export const str = (exact: string): Pattern => state => {
	const expectedLen = exact.length;
	const remainingLen = state.chunk.length - state.index;

	if (remainingLen < expectedLen) {
		const actual = state.chunk.substring(state.index, state.index + remainingLen)

		throw new ParserError(
			state,
			`Expected '${prettyChars(exact)}' but got '${prettyChars(actual)}'`
		);
	}

	if (state.chunk.startsWith(exact, state.index)) {
		return appendState(
			state,
			[exact],
			state.index + expectedLen
		);

	} else {
		const actual = state.chunk.substring(state.index, state.index + expectedLen);
		throw new ParserError(
			state,
			`Expected '${prettyChars(exact)}' but got '${prettyChars(actual)}'`
		)
	}
}