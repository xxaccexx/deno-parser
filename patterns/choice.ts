import { ParserError, appendState, emptyState } from "../state.ts";
import { Pattern } from "../parser.ts";

/**
 * Follow the first pattern that passes successfully.
 * key feature is patterns that return the same state, like optional, are also skipped.
 */
export const choice = (firstPattern: Pattern, ...otherPatterns: Pattern[]): Pattern => {
	const patterns = [firstPattern, ...otherPatterns];

	return state => {
		let firstError: undefined | ParserError = undefined;

		for (const pattern of patterns) {
			try {
				const nState = pattern(emptyState(state));
				return appendState(state, nState.results, nState.index);
			}
			catch (e) {
				if (!firstError) firstError = e as ParserError;
				continue;
			}
		}

		throw new ParserError(
			state,
			'No patterns matched'
		)
	}
}


// const choice_ = (...parserList: Pattern[]): Pattern => state => {
// 		if (state.error) return state;
// 		for (const parser of parserList) {
// 			self.pattern = parser.pattern;

// 			const newS tate = parser(state);
// 			if (newState.error || state == newState) continue;
// 			return newState;
// 		}

// 		return updateParserError(state, `No choice was matched`);
// 	}

// 	self.pattern = '';
// 	return self;
// }