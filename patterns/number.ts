import { join } from "../operations/mod.ts";
import { chain } from "./chain.ts";
import { choice } from "./choice.ts";
import { many } from "./many.ts";
import { str } from "./string.ts";
import { until } from "./until.ts";


export const binChar = () => choice(
	str('0'),
	str('1')
)

export const numChar = () => choice(
	binChar(),
	str('2'),
	str('3'),
	str('4'),
	str('5'),
	str('6'),
	str('7'),
	str('8'),
	str('9'),
);

export const hexChar = () => choice(
	numChar(),
	str('a'), str('A'),
	str('b'), str('B'),
	str('c'), str('C'),
	str('d'), str('D'),
	str('e'), str('E'),
	str('f'), str('F'),
)

export const exponent = () => join(
	chain(
		str('e'),
		until(
			numChar(),
			str(' ')
		)
	)
)

export const integer = () => join(
	many(numChar())
);

export const decimals = () => join(
	chain(
		str('.'),
		many(numChar())
	)
)

export const hex = () => join(
	chain(
		str('0x'),
		many(hexChar())
	)
)

export const bin = () => join(
	chain(
		str('0b'),
		many(binChar())
	)
)