import { Pattern } from "../parser.ts";

export const optional = (pattern: Pattern): Pattern => state => {
	try { return pattern(state) }
	catch { return state }
}