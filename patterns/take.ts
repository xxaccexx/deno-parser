import { Pattern } from "../parser.ts";
import { ParserError, appendState } from "../state.ts";

export const take = (count: number = 1): Pattern => state => {
	const remainingLen = state.chunk.length - state.index;
	if (remainingLen < count) {
		throw new ParserError(
			state,
			`Expected ${count} chars, only got ${remainingLen}`
		);
	}

	const result = state.chunk.substring(state.index, state.index + count);
	return appendState(state, [result], state.index + count);
}

take()