import { Pattern } from "../parser.ts";
import { ParserState, replaceState, emptyState } from "../state.ts";

export const chain = (firstPattern: Pattern, ...otherPatterns: Pattern[]): Pattern => {
	const patterns = [firstPattern, ...otherPatterns];

	return (state: ParserState) => {
		const results: any[] = [];
		let nState = emptyState(state);

		for (const pattern of patterns) {
			const s = pattern(nState);
			results.push(s.results);
			nState = emptyState(s)
		}

		return replaceState(nState, [...state.results, ...results], nState.index);
	}
}